package main

import "os"

import "gitlab.com/lead-scm/pb/cli"

func main() {
	os.Exit(cli.Main())
}
